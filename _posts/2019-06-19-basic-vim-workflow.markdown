---
title: Vim IDE \\ Overly Simplified Layout for Code Editing
layout: post
excerpt: "My needs are very basic but for web development this quick and dirty layout works great for me."
slug: basic-vim-workflow
---
<video width="640" height="360" controls poster="https://archive.org/download/basic-vim-workflow-web/vim.jpg">
  <source src="https://archive.org/download/basic-vim-workflow-web/vim-sm.webm">
</video>

My needs are very basic but for web development this quick and dirty layout works great for me.