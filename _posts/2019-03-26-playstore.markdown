---
title: The Playstore is Less Relevant than Ever
layout: post
excerpt: "Not only is it important to ditch Google in mobile but it's also never been easier."
slug: playstore
---
When the first Android device was announced I was excited to see a phone powered by Linux that actually had a chance to capture the market. I was naive thinking that everything that I loved about Linux would be on the forefront of this new mobile operating system.

What I got instead was a very locked down environment. Custom proprietary launchers and application. Facebook installed by default without a way to free up the very limited space early phones had.

The Nexus project gave me a spark of joy. At the time it was the closest you could get to an Open Source phone. The AOSP builds caused a chain reaction with independent developers building new and interesting firmware for hundreds of phones.

It wasn't until recently that both the open firmware's like Linage OS and the Micro G projects matured enough that they could be solid drop in replacements for the traditional Google/Samsung offerings.

+ Firefox Web Browser: The best mobile web browser
+ QKSMS: SMS Application with tons of features
+ NextCloud + DAVX: Cloud storage, Calendar, and Contacts
+ OsmAnd: Maps and navigation
+ Termux: Full Linux userland
+ Riot: Federated Chat
+ Tusky: Mastodon client
+ Markor: Markdown text editor for notes

Most of the things I need my phone for are tools from F-Droid.

I finally upgraded to Linage OS 16 which is built on Android 9 Pie. New tech allowed applications from the Play Store to be broken up into parts that could speed up the upgraded process but ultimately broke some compatibility with Play Store front ends like Yalp. For a while it seemed like Yalp was the only way to grab those last little proprietary apps that we sometimes need.

I found myself installing from the Amazon app store. It actually had a very large collection of popular apps. I could almost get all my property apps from there and call it a day. The dependence on Google for a complete mobile experience is very quickly coming to an end.

In reality I can do 95% of my mobile work with only F-Droid installed. Most proprietary apps have decent alternatives or usable web versions available.

We haven't even started talking about web apps. I use the web version of Google's maps to look up bus times. I use the web version of Instagram to share photos. I use the web version of my credit card and bank account. There is no reason these applications should take up space on my device if I only use them periodically. It's not like they have functions that can't be generated in a good web app.

The thing to take away is not how much we need to remove Google. I've gone on about that for a long time and I'm sure more are kinda board of hearing it. The thing that gets me is that it's really never been easier.

Kinda like that Linux thing.
