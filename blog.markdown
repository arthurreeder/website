---
layout: default
title: Blog
slug: /blog/
---

<div class="blog">

  <h1 class="page-heading">Posts</h1>
  <p>
    I try to post regularly but sometimes it's hard to keep on the ball. I also have an <a href="{{ "/feed.xml" | prepend: site.baseurl }}">RSS Feed</a> for those who use an RSS reader.
  </p>

  <ul class="post-list">
    {% for post in site.posts %}
      <li>
        <span class="post-meta">{{ post.date | date: "%b %-d, %Y" }}</span>

        <h2>
          <a class="post-link" href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a>
        </h2>
      </li>
    {% endfor %}
  </ul>

</div>